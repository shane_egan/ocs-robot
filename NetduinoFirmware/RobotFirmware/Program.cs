﻿using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;
using System.IO.Ports;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using SecretLabs.NETMF.Hardware;
using SecretLabs.NETMF.Hardware.NetduinoPlus;



namespace RobotFirmware
{

    public enum directions { left, right, forward, reverse, stop };

    static class Constants
    {
        public const uint Motorspeed = 100;
        public const int Baudrate = 9600;
    }

    public static class moveState_next
    {
        static public int cmd_number = 0;
        static public directions direction = directions.stop;
        static public int direction_time = -1;
    }

    public static class moveState_current
    {
        static public int cmd_number = 0;
        static public directions direction = directions.stop;
        static public int direction_time = -1;
        static public bool done = false;
    }
    


    public static class Program
    {
        static SerialPort ttlSerial;


        static PWM pwmMotorLeft = new PWM(Pins.GPIO_PIN_D5);
        static PWM pwmMotorRight = new PWM(Pins.GPIO_PIN_D6);
        

        static OutputPort dirMotorLeft = new OutputPort(Pins.GPIO_PIN_D3, false);
        static OutputPort dirMotorRight = new OutputPort(Pins.GPIO_PIN_D4, false);

        static OutputPort led1 = new OutputPort(Pins.ONBOARD_LED, false);
        


        static void move_robot(directions direction)
        {
            //Debug.Print(direction.ToString());

            pwmMotorLeft.SetDutyCycle(0);
            pwmMotorRight.SetDutyCycle(0);

            switch (direction)
            {
                case directions.forward:
                    Debug.Print("Forward CMD");
                    dirMotorLeft.Write(true);
                    dirMotorRight.Write(true);
                    break;
                case directions.left:
                    Debug.Print("Left CMD");
                    dirMotorLeft.Write(false);
                    dirMotorRight.Write(true);
                    break;
                case directions.reverse:
                    Debug.Print("Reverse CMD");
                    dirMotorLeft.Write(false);
                    dirMotorRight.Write(false);
                    break;
                case directions.right:
                    Debug.Print("Right CMD");
                    dirMotorLeft.Write(true);
                    dirMotorRight.Write(false);
                    break;
                case directions.stop:
                    Debug.Print("Stop CMD");
                    break;
            }

            if (direction != directions.stop)
            {
                pwmMotorLeft.SetDutyCycle(Constants.Motorspeed);
                pwmMotorRight.SetDutyCycle(Constants.Motorspeed);
            }

        }

        private static Timer cmdTimer;
        static TimerCallback cmdTimerDelegate = new TimerCallback(Program.StopRobot);



        public static void StopRobot(Object stateinfo)
        {
            led1.Write(!led1.Read());
            move_robot(directions.stop);
            moveState_current.done = true;
        }

        public static void Main()
        {
            ttlSerial = new SerialPort(SerialPorts.COM1, Constants.Baudrate);
            ttlSerial.Open();
            ttlSerial.DataReceived += new SerialDataReceivedEventHandler(serial_DataReceived);

            cmdTimer = new Timer(cmdTimerDelegate, null, 0, Timeout.Infinite);


            while (true)
            {

                if (moveState_current.cmd_number != moveState_next.cmd_number)
                {
                    moveState_current.direction = moveState_next.direction;

                    if (moveState_next.direction_time == -1)
                        cmdTimer.Change(Timeout.Infinite, Timeout.Infinite);
                    else //Set a timer to send the STOP command
                    {
                        cmdTimer.Change(moveState_next.direction_time, moveState_next.direction_time);
                        moveState_current.done = false;
                    }

                    moveState_current.direction_time = moveState_next.direction_time;
                    moveState_current.cmd_number = moveState_next.cmd_number;
                    move_robot(moveState_current.direction);
                }
                else //commands are the same
                {
                    if (moveState_current.done == true)
                    {
                        cmdTimer.Change(Timeout.Infinite, Timeout.Infinite);
                    }
                }

            }
        }

        static void signal_move_robot(char dir, int time)
        {
            switch (dir)
            {
                case 'F': moveState_next.direction = directions.forward;
                    break;
                case 'L': moveState_next.direction = directions.left;
                    break;
                case 'R': moveState_next.direction = directions.right;
                    break;
                case 'B': moveState_next.direction = directions.reverse;
                    break;
                case 'S': moveState_next.direction = directions.stop;
                    break;
                default:
                    Debug.Print("No direction found, def: STOP");
                    moveState_next.direction = directions.stop;
                    break;
            }

            moveState_next.direction_time = time;
            moveState_next.cmd_number++;

        }

        static void validate_command(string command)
        {
            char[] cmd = command.ToCharArray();
            string time;
            int iTime = -1;

            if (cmd.Length < 3)
            {
                Debug.Print("Length too short");
                return;
            }

            if (cmd[0] != 'M')
            {
                Debug.Print("Not M");
                return;
            }

            if (cmd.Length == 3)
            {
                signal_move_robot(cmd[2], iTime);
                return;
            }
            else if (cmd.Length >= 5)
            {

                time = new string(cmd, 4, cmd.Length - 4);
                Debug.Print("Time: ");
                Debug.Print(time);
                try
                {

                    iTime = int.Parse(time);
                }
                catch (Exception)
                {
                    Debug.Print("Time string not valid");
                    return;
                }
                signal_move_robot(cmd[2], iTime);
                return;
            }
            else
            {
                Debug.Print("Invalid Move CMD");
            }
        }

        static bool command_start = false;
        static int command_len = 0;
        static string strCommand;

        static void process_command(byte in_byte)
        {
            if (in_byte == '>')
            {
                command_start = true;
                command_len = 0;
                strCommand = "";
                return;
            }

            if (command_start && in_byte == '\r')
            {
                Debug.Print(strCommand);
                validate_command(strCommand);
                command_len = 0;
                strCommand = "";
                command_start = false;
                return;
            }

            if (command_len > 32)
            {
                Debug.Print("Junk Disgarded\r\n");
                command_len = 0;
                strCommand = "";
                command_start = false;
                return;
            }

            command_len++;
            strCommand += (char)in_byte;

        }

        static void serial_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {

            // create a single byte array
            byte[] bytes = new byte[1];
            string strLine;

            // as long as there is data waiting to be read
            while (ttlSerial.BytesToRead > 0)
            {
                // read a single byte
                ttlSerial.Read(bytes, 0, bytes.Length);

                process_command(bytes[0]);

                try
                {
                    strLine = new String(System.Text.Encoding.UTF8.GetChars(bytes));
                }
                catch (Exception)
                {
                    strLine = "Exception Caugth";
                }

                ttlSerial.Write(bytes, 0, bytes.Length);
            }

        }

    }
}
