﻿namespace TCP_to_COM
{
    partial class GUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_start = new System.Windows.Forms.Button();
            this.lb_console = new System.Windows.Forms.Label();
            this.btn_stop = new System.Windows.Forms.Button();
            this.rTB_DebugConsole = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // btn_start
            // 
            this.btn_start.BackColor = System.Drawing.Color.LimeGreen;
            this.btn_start.Location = new System.Drawing.Point(17, 12);
            this.btn_start.Name = "btn_start";
            this.btn_start.Size = new System.Drawing.Size(103, 33);
            this.btn_start.TabIndex = 0;
            this.btn_start.Text = "Start";
            this.btn_start.UseVisualStyleBackColor = false;
            this.btn_start.Click += new System.EventHandler(this.btn_start_Click);
            // 
            // lb_console
            // 
            this.lb_console.AutoSize = true;
            this.lb_console.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_console.Location = new System.Drawing.Point(14, 86);
            this.lb_console.Name = "lb_console";
            this.lb_console.Size = new System.Drawing.Size(71, 18);
            this.lb_console.TabIndex = 2;
            this.lb_console.Text = "Console";
            // 
            // btn_stop
            // 
            this.btn_stop.BackColor = System.Drawing.Color.Red;
            this.btn_stop.Location = new System.Drawing.Point(241, 12);
            this.btn_stop.Name = "btn_stop";
            this.btn_stop.Size = new System.Drawing.Size(103, 33);
            this.btn_stop.TabIndex = 3;
            this.btn_stop.Text = "Stop";
            this.btn_stop.UseVisualStyleBackColor = false;
            this.btn_stop.Click += new System.EventHandler(this.btn_stop_Click);
            // 
            // rTB_DebugConsole
            // 
            this.rTB_DebugConsole.Location = new System.Drawing.Point(17, 107);
            this.rTB_DebugConsole.Name = "rTB_DebugConsole";
            this.rTB_DebugConsole.Size = new System.Drawing.Size(327, 395);
            this.rTB_DebugConsole.TabIndex = 4;
            this.rTB_DebugConsole.Text = "";
            // 
            // GUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(362, 514);
            this.Controls.Add(this.rTB_DebugConsole);
            this.Controls.Add(this.btn_stop);
            this.Controls.Add(this.lb_console);
            this.Controls.Add(this.btn_start);
            this.Name = "GUI";
            this.Text = "TCP to COM";
            this.Closed += new System.EventHandler(this.GUI_Closed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_start;
        private System.Windows.Forms.Label lb_console;
        private System.Windows.Forms.Button btn_stop;
        private System.Windows.Forms.RichTextBox rTB_DebugConsole;

    }
}

