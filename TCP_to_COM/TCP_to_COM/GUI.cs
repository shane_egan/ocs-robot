﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using TcpLib;

namespace TCP_to_COM
{
    public delegate void debugMessageDel(MessageEventArgs e);

    public partial class GUI : Form
    {

        workerThread thread;
        private bool serverStarted = false;

        public GUI()
        {
            InitializeComponent();
            thread = new workerThread();
            btn_stop.Enabled = false;
            //thread.debugMessage += new debugMessageDel(thread_outputDebug);
        }

        private void btn_start_Click(object sender, EventArgs e)
        {
            thread.Start();

            thread_outputDebug("**Start Server**", DebugSrc.Debug);
            btn_start.Enabled = false;
            btn_stop.Enabled = true;
        }

        private void GUI_Closed(object sender, System.EventArgs e)
		{
            if(serverStarted)
                thread.Stop();
		}

        private void btn_stop_Click(object sender, EventArgs e)
        {
            
            thread.Stop();
            thread_outputDebug("**Stop Server**", DebugSrc.Debug);
            serverStarted = false;
            btn_stop.Enabled = false;
            btn_start.Enabled = true;
        }


        private void outputDebug(MessageEventArgs e)
        {
            rTB_DebugConsole.AppendText(e.Message);    
        }

        public enum DebugSrc {Client, Server, Debug};

        public void thread_outputDebug(string debugStr, DebugSrc src)
        {
            //MessageBox.Show("Stuff happened!");
            //can tweak message evnts class to change output colour and stuff ;) 
            if (InvokeRequired)
                Invoke(new Action<string, DebugSrc>(thread_outputDebug), debugStr, src);
            //Invoke(new Action<string>(thread_outputDebug), debugStr);
            else
            {
                if (src == DebugSrc.Client)
                    rTB_DebugConsole.SelectionColor = Color.Aqua;
                else if (src == DebugSrc.Server)
                    rTB_DebugConsole.SelectionColor = Color.LimeGreen;
                else
                    rTB_DebugConsole.SelectionColor = Color.Magenta;

                rTB_DebugConsole.AppendText(debugStr);
                rTB_DebugConsole.AppendText("\r\n");
            }
        }
    }


    //Runs in a background thread
    public class workerThread
    {
        Thread worker;
        private bool _quit = false;

        private TcpServer Server;
        private TCPWrapper Provider;
        


        /*  I don't think this next line is correct*/
        //public event debugMessageDel debugMessage;


        //protected virtual void sendDebugEvent(MessageEventArgs e)
        //{
        //    if (debugMessage != null)
        //        debugMessage(e);
        //}


        public void Start()
        {
            ThreadStart start = new ThreadStart(Run);
            worker = new Thread(start);
            Provider = new TCPWrapper();
            //Provider.debug_Message += new debugMessageDel(Provider_debug_Message);
            Server = new TcpServer(Provider, 555);
            Server.Start();
            worker.Start();

        }

        public void Stop()
        {
            Server.Stop();
            worker.Abort();
        }



        private void Run()
        {
            //int i = 0;

            while (!_quit)
            {
                Thread.Sleep(1000);
                //i++;

               //debugMessage(new MessageEventArgs(false, "it worked!"));
               // Console.WriteLine(string.Format("Slept {0} seconds.", i));
            }
            Console.WriteLine("Thread exiting");
        }
    }


    public class MessageEventArgs : EventArgs
    {
        public MessageEventArgs(bool Error, string message)
        {
            IsError = Error;
            Message = message;
        }

        public bool IsError { get; set; }
        public string Message { get; set; }
    }
}
