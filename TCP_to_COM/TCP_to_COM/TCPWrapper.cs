using System;
using System.Text;
using System.Windows.Forms;
using TcpLib;
using System.IO.Ports;

namespace TCP_to_COM
{
	/// <SUMMARY>
	/// EchoServiceProvider. Just replies messages received from the clients.
	/// </SUMMARY>
	public class TCPWrapper: TcpServiceProvider
	{
		private string _receivedStr;
        public event debugMessageDel debug_Message;
        static SerialPort _serialPort;

		public override object Clone()
		{
			return new TCPWrapper();
		}

		public override void OnAcceptConnection(ConnectionState state)
		{
            Console.WriteLine("New Connection");
			_receivedStr = "";

            if (_serialPort == null)
            {
                Console.WriteLine("Setting up serial");
                _serialPort = new SerialPort();
                _serialPort.PortName = "COM22"; // "COM22";
                _serialPort.BaudRate = 9600;
                _serialPort.ReadTimeout = 1000;
                _serialPort.WriteTimeout = 1000;
                _serialPort.Open();
            }

			//if(!state.Write(Encoding.UTF8.GetBytes("Hello World!\r\n"), 0, 14))
			//	state.EndConnection(); //if write fails... then close connection
		}


        //private string SendToCom(string data)
        private void SendToCom(string data)
        {
            Console.WriteLine("Send serial data");
            if(!_serialPort.IsOpen)
                _serialPort.Open();

            try
            {
                _serialPort.WriteLine(data);
            }
            catch (Exception)
            { }
            //return _serialPort.ReadLine();
        }


		public override void OnReceiveData(ConnectionState state)
		{
			byte[] buffer = new byte[1024];
            string str_com, str_reply;

			while(state.AvailableData > 0)
			{
				int readBytes = state.Read(buffer, 0, 1024);
                if (readBytes > 0)
                {
                    _receivedStr += Encoding.UTF8.GetString(buffer, 0, readBytes);
                    Console.WriteLine("TCP_data recieved");
                    Console.WriteLine(_receivedStr);
                    //if(_receivedStr.IndexOf('\r') >= 0)
                    //{
                    //state.Write(Encoding.UTF8.GetBytes(_receivedStr), 0,
                    //	_receivedStr.Length);
                    Program.myGUI.thread_outputDebug(_receivedStr, GUI.DebugSrc.Client);
                    SendToCom(_receivedStr);
                    //str_com = SendToCom(_receivedStr);
                    /*
                    if (str_com.Contains("OK"))
                    {
                        str_reply = "OK";
                        try
                        {
                            state.Write(Encoding.UTF8.GetBytes(str_reply), 0, str_reply.Length);
                            Program.myGUI.thread_outputDebug(str_reply, GUI.DebugSrc.Server);
                        }
                        catch
                        {
                            Program.myGUI.thread_outputDebug("Client closed connection", GUI.DebugSrc.Debug);
                        }
                            
                    }
                    else
                    {
                        str_reply = "Error";
                        try
                        {
                            state.Write(Encoding.UTF8.GetBytes(str_reply), 0, str_reply.Length);
                            Program.myGUI.thread_outputDebug(str_reply, GUI.DebugSrc.Server);
                        }
                        catch
                        {
                            Program.myGUI.thread_outputDebug("Client closed connection", GUI.DebugSrc.Debug);
                        }
                    }
                    */
                    _receivedStr = "";

                    //str_reply = "";
                    //}
                }
                else
                {
                    state.EndConnection(); //If read fails then close connection
                    Console.WriteLine("Ending Connection");
                    _serialPort.Close();
                }

			}
            state.EndConnection();
            _serialPort.Close();
		}


		public override void OnDropConnection(ConnectionState state)
		{
			//Nothing to clean here
            Console.WriteLine("Connection Dropped");
            try
            {
                _serialPort.Close();
            }
            catch (Exception)
            { }
		}
	}
}
