<?php

require_once __DIR__.'/bootstrap.php';


$app->get('/', function () use ($app) {
    return $app['twig']->render('login.twig');
});


$app->get('/access', function () use ($app) {
    return $app['twig']->render('access.twig');
});

$app->mount('/api', new T360\Route\ApiControllerProvider());

$app->run();