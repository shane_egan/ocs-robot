<?php

require_once __DIR__.'/../vendor/autoload.php';

$app = new Silex\Application();

//config (app env set in htaccess)

if (getenv('APPLICATION_ENV') === 'development') {
    $app['debug'] = true;   // Debug
    $configFile = 'config.dev.yml';
    $app->register(new Whoops\Provider\Silex\WhoopsServiceProvider);
} else {
    $configFile = 'config.prod.yml';
}

$app->register(
    new Igorw\Silex\ConfigServiceProvider(__DIR__ . '/' . $configFile)
);

$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/../web/views',
));

$app->register(new Whoops\Provider\Silex\WhoopsServiceProvider);

$app->register(new Silex\Provider\MonologServiceProvider(), array(
    'monolog.logfile' => __DIR__.'/development.log',
));

$app['db'] = new Zend\Db\Adapter\Adapter(array(
    'driver'   => 'Mysqli',
    'host'     => 'localhost',
    'username' => 'root',
    'password' => '',
    'dbname'   => 'robot',
    'options' => array('buffer_results' => true)
));