<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ApiControllerProvider
 *
 * @author ShaneDesk
 */

namespace T360\Route;

use T360\Controllers;
use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ApiControllerProvider implements ControllerProviderInterface {

    public function connect(Application $app) {
        // creates a new controller based on the default route
        $controllers = $app['controllers_factory'];
        //http://silex.sensiolabs.org/doc/cookbook/json_request_body.html
        //Content-Type : application/json
        //{"username":"JSONuser","password":"pass"}
        
        $app->before(function (Request $request) {
             
            //do validation to check if user is allowed access here 
        if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
                $data = json_decode($request->getContent(), true);
                $request->request->replace(is_array($data) ? $data : array());
            }
        });
        
        
        $controllers->get('/test/{dir}', function ($dir) use ($app) {
                    //$robotController = new Controllers\Api\RobotController($app['db']);
                    //$robot = $robotController->move($dir);
                    
                    return new Response("Successful Move $dir",202);
                    
                    if($robot == Null) //no users
                        return new Response('',204);
                    
                    return $app->json($robot, 200);
                }); 
                
        $controllers->get('/access', function(Request $request) use ($app){
                    $robotController = new Controllers\Api\RobotController($app['db']);
                    $ip = $request->getClientIp();
                    
                    $statCode = $robotController->doesUserHaveAccess($ip);
                    
                    return new Response("",$statCode);
                });
        $controllers->get('/giveaccess', function(Request $request) use ($app){
                    $robotController = new Controllers\Api\RobotController($app['db']);
                    $params = $request->query->all();
                    var_dump($params);
                    
                    if(isset($params['ip']))
                    {
                        $robotController->giveAccess($params['ip']);
                        return new Response("Successful operation",200);
                    }
                    return new Response("Failed operation",400);
                });
                
        $controllers->get('/getusers', function(Request $request) use ($app){
                    $robotController = new Controllers\Api\RobotController($app['db']);
                    $result = $robotController->getAllUsers();
                    
                    //var_dump($result);
                    
                    return $app->json($result, 200);
                    //return new Response("Failed operation",400);
                });          
           
                
        $controllers->get('/register', function(Request $request) use ($app){
                    $robotController = new Controllers\Api\RobotController($app['db']);
                    $params = $request->query->all();
                    var_dump($params);
                    $ip = $request->getClientIp();
                    
                    if(!isset($params['user']))
                            return new Response("Username not set", 400); 
                    
                    if($robotController->isUserRegistered($ip))
                    {
                        echo "</br>User is registered";
                        
                        if(!isset($params['user']))
                            return new Response("Username not sent", 400); 
                        
                        $userRSP = $robotController->checkUpdateUsername($params['user'], $ip);
                        return new Response("", $userRSP);                        
                    }
                    else 
                    {
                        echo "</br>User not registered, Registering...";
                        $userRSP = $robotController->addUser($params, $ip);
                        
                        if($userRSP)
                            return new Response("Username added", 200);
                        else
                            return new Response("Username Conflict: username has to be unique", 409);
                    }
                });  
                
        $controllers->get('/move', function(Request $request) use ($app){
                    $robotController = new Controllers\Api\RobotController($app['db']);
                    $params = $request->query->all();
                    var_dump($params);
                    
                    $ip = $request->getClientIp();
                    
                    $stat = $robotController->move_params($params, $ip);
                    
                    if($stat['errorFlag'])
                        return new Response("Failed move",$stat['errorCode']);
                    else
                        return new Response("Successful move IP: $ip",200);
                });  
                
        $controllers->post('/move', function(Request $request) use ($app){
                    $robotController = new Controllers\Api\RobotController($app['db']);
                    $params = $request->request->all();
                    //$user = $request->request->all();
                    var_dump($params);
                    
                    $ip = $request->getClientIp();
                    $stat = $robotController->move_params($params, $ip);
                    
                    if($stat['errorFlag'])
                        return new Response("Failed Move",$stat['errorCode']);
                    else
                        return new Response("Successful Move IP: $ip",200);
                });                  


        
        /*
        $controllers->get('/user', function () use ($app) {
                    $userController = new Controllers\Api\UserController($app['db']);
                    $users = $userController->getAllUsers();
                    
                    if($users == Null) //no users
                        return new Response('',204);
                    
                    return $app->json($users, 200);
                });
        
        $controllers->get('/user/{id}', function ($id) use ($app) {
                    $userController = new Controllers\Api\UserController($app['db']);
                    $user = $userController->getUser($id);
                    if($user->iduser == Null) //no user found
                        return new Response('',204);
                    
                    return $app->json($user, 200);
                });
                
        $controllers->post('/user', function(Request $request) use ($app){
                    $userController = new Controllers\Api\UserController($app['db']);
                    $user = $request->request->all();
                    try{
                        $userModel = $userController->addUser($user);
                    }
                    catch(\Exception $e){
                        return new Response($e->getMessage(),409);
                    }
                    return $app->json($userModel->toArray(), 200); //was 201 but changed to suit x-editable
                });  
         * 
         */
       /*$controllers->delete('/user/{id}', function ($id) use ($app){
                    $userController = new Controllers\Api\UserController($app['db']);
                    $userController->delUser($id);
                    return new Response('',200);
                });*/
        

        return $controllers;
    }

}