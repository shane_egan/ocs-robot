<?php
namespace T360\Controllers\Api;
//use T360\Models\Factories;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AuthController
 *
 * @author ShaneDesk
 */

use Zend\Db\TableGateway\TableGateway;

class RobotController {
    
    private $db;
    var $userTable;
    
    public function __construct($db) {
        $this->db = $db;
        $this->userTable = new TableGateway('users', $db);
    }
    
    public function updateAccess($ip, $access)
    {
        $usersModel['allowedAccess'] = $access;
        $where = array('ip = ?' => $ip);
        try
        {
            $this->userTable->update($usersModel, $where);
        }
        catch (\Exception $e)
        {
            echo $e->getMessage();
            return false;
        }
        
        return true;
        
    }
    
    public function getAllUsers()
    {
        $rowset = $this->userTable->select();  
        foreach($rowset as $row)
        {
            $users[] = $row;
        }
        
        if(isset($users))
            return $users;
        
        return Null;
    }
    
    public function giveAccess($ip)
    {
        $rowset = $this->userTable->select(array('allowedAccess' => 1));
        foreach($rowset as $row)
        {
            $this->updateAccess($row['ip'], 0); //Set all access bits to 0
        }
        $this->updateAccess($ip, 1);
    }
    
    public function doesUserHaveAccess($ip)
    {
        if($this->isUserRegistered($ip))
        {
            return $this->checkUser($ip);
        }
        else
        {
            return 401; //not registered
        }
    }
    
    public function isUserRegistered($ip)
    {
        $rowset = $this->userTable->select(array('ip' => $ip));
        //$row = $rowset->current();
        $rowCount = count($rowset);
        
        if($rowCount == 0)
            return false;
        
        return true;
        
    }
    
    private function updateUsername($username, $ip)
    {
        $usersModel['username'] = $username;
        $where = array('ip = ?' => $ip);
        try
        {
            $this->userTable->update($usersModel, $where);
        }
        catch (\Exception $e)
        {
            echo $e->getMessage();
            return false;
        }
        
        return true;
        
    }
    
    public function checkUpdateUsername($username, $ip)
    {
        $rowset = $this->userTable->select(array('ip' => $ip));
        $row = $rowset->current();
        $rowCount = count($rowset);
        
        if($rowCount == 0)
        {
            //echo "</br>User not found";
            return 400; //error user not found
        }
        
        if($row['username'] == $username)
        {
            //echo "</br>Username matches existing";
            return 200; //username OK
        }
        else
        {
            if($this->updateUsername($username, $ip))
            {
                //echo "</br>Username updated";    
                return 201; //user updated
            }
            else
            {
                return 409; //conflicted username
            }
        }
    }
    
    
    public function addUser($params, $ip)
    {
        $usersModel['username'] = $params['user'];
        $usersModel['ip'] = $ip;
        $usersModel['allowedAccess'] = 0;
        
        try{
            $this->userTable->insert($usersModel);
        }
        catch (\Exception $e)
        {
            echo $e->getMessage();
            return false;
        }
        
        return true;
    }
    
    
    public function checkUser($ip)
    {
        
        $rowset = $this->userTable->select(array('ip' => $ip));
        $row = $rowset->current();
        $rowCount = count($rowset);
        
        echo "User Query Result ($rowCount)</br>";
        echo "Index: " . $row['index'];
        echo "</br>";
        echo "Username: " . $row['username'];
        echo "</br>";
        echo "IP: " . $row['ip'];
        echo "</br>";
        echo "Access: " . $row['allowedAccess'];
        echo "</br>";
        
        if($rowCount == 0)
            return 401;
        
        if($row['allowedAccess'] != 1)
            return 405;
        
        return 200;
    }
    
    
    public function move($dir, $len = -1){
        $fp = fsockopen("tcp://localhost", 555, $errno, $errstr, 30);
        if (!$fp) {
            echo "$errstr ($errno)<br />\n";
        } else {
            
            if($len > -1)
                $out = ">M $dir $len\r";
            else
                $out = ">M $dir\r";
            //$out .= "echo server\r";
            fwrite($fp, $out);
            //while (!feof($fp)) {
            //    echo fgets($fp, 128);
            //}
            fclose($fp);
        }
        
    }
    
    public function move_params($params, $ip)
    {   
        $status['errorFlag'] = false;
        $status['message'] = "";
        $status['errorStr'] = "";
        
        $userResult = $this->checkUser($ip);
        echo $userResult;
        
        if($userResult != 200) //OK 
        {
            $status['errorFlag'] = true; 
            $status['errorCode'] = $userResult;
            return $status;
        }
        
        $len_local = -1;
        if(isset($params['len']))
        {
            $len_local = $params['len'];
        }
        
        if(isset($params['dir']))
        {
            if($params['dir'] == "left")
            {
                $status['message'] .= "dir = left";
                $this->move('L', $len_local);
            }
            else if($params['dir'] == "right")
            {
                $status['message'] .= "dir = right";
                $this->move('R', $len_local);
            }
            else if($params['dir'] == "forward")
            {
                $status['message'] .= "dir = forward";
                $this->move('F', $len_local);
            }
            else if($params['dir'] == "reverse")
            {
                $status['message'] .= "dir = reverse";
                $this->move('B', $len_local);
            }
            else if($params['dir'] == "stop")
            {
                $status['message'] .= "dir = stop";
                $this->move('S');
            }
            else
            {
                $status['errorFlag'] = true; 
                $status['errorCode'] = 400;
                $status['errorStr'] .= "incorrect dir parameter ";
            }
        }
        else {
            $status['errorFlag'] = true;
            $status['errorCode'] = 400;
            $status['errorStr'] .= "dir index not found ";
        }
        
        
        return $status;
        
    }
/*    
    public function getUser($id){
        $userFactory = new Factories\UserFactory($this->db);
        return $userFactory->getById($id);
    }
    
    public function getAllUsers(){
        $userFactory = new Factories\UserFactory($this->db);
        return $userFactory->getAll();
    }
    
    public function addUser(array $user){
        
        $userModel = new \T360\Models\UserModel($user);
        $userFactory = new Factories\UserFactory($this->db);
        try{
            return $userFactory->add($userModel);
        }
        catch (\Exception $e)
        {
            throw $e;
        }
    }
    
    public function delUser($id){
        $userFactory = new Factories\UserFactory($this->db);
        $userFactory->deleteById($id);
    }
 * 
 */
}

?>
