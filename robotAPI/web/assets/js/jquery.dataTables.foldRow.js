/* Formating function for row details */
function fnFormatDetails(oTable, nTr)
{
	//could use this to send which unit ID we want to a php file to retrieve the data 
	//var aData = oTable.fnGetData(nTr);
	var sOut = '<tr><td>Extra Unit Information</td><td></td><td></td><td></td></tr>';

	return sOut;
}

/* Table initialisation */
$(document).ready(function() {
	var oTable = $('#example').dataTable({
		"bRetrieve": true
	});


	/*
	 * Initialse DataTables, with no sorting on the 'details' column
	 
	var oTable = $('#example').dataTable({
		"aoColumnDefs": [
			{"bSortable": false, "aTargets": [0]}
		],
		"aaSorting": [[1, 'asc']]
	});*/

	/* Add event listener for opening and closing details
	 * Note that the indicator for showing which row is open is not controlled by DataTables,
	 * rather it is done here
	 */
	//$('#example tbody td img').live('click', function() {
	$('#example tbody td .foldbutton').live('click', function() {
		var nTr = this.parentNode.parentNode;
		console.log(this.className);
		if (this.className.match('icon-chevron-up'))
		{
			console.log("Close the row");
			/* This row is already open - close it */
			this.className = "icon-chevron-down foldbutton align-right";
			oTable.fnClose(nTr);
		}
		else
		{
			console.log("Open the row");
			/* Open this row */
			this.className = "icon-chevron-up foldbutton align-right";
			oTable.fnOpen(nTr, fnFormatDetails(oTable, nTr), 'details');
		}
	});
});
