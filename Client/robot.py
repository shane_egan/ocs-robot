import sys
if sys.version_info >= (3, 0):
    # python 3
    import tkinter as tk
    from tkinter import ttk
    import tkinter.messagebox
    import http.client as httplib
    import urllib.parse as urllib
else:
     # python 2
    import Tkinter as tk
    import tkMessageBox # tkMessageBox beomes tkinter.messagebox in python 3
    import httplib, urllib
    import ttk



################################################################################
def msgbox(msg, title = "Robot"):
    if sys.version_info >= (3, 0):
        tk.messagebox.showinfo(title, msg) # Python 3
    else:
        tkMessageBox.showinfo(title, msg) # Python 2
################################################################################
def server_request(request_string):
    """ Sends a HTTP GET request to the server and gets a response """
    debug_mode = False # When True this allows us to test without a server by just looking at the print output

    print("server_request( " + request_string + " )")
    if debug_mode:
        return None
    conn = httplib.HTTPConnection("10.0.2.213", 80)
    conn.request("GET", request_string)
    response = conn.getresponse()
    print("Server Response: status: " + str(response.status) + ". Reason: " + response.reason + ".")
    #response.read()
    conn.close()
    return response
################################################################################
def send_direction_request_to_robot_server(direction, length = None):
    request_string =  "/robot/api/move?dir=" + direction
    if length:
        request_string = request_string + "&len=" + str(length)
    return server_request(request_string)
################################################################################
def enable_all_direction_buttons(state = True):
    if state:
         button_state = tk.NORMAL
    else:
         button_state = tk.DISABLED
    for btn in  (btn_left, btn_right, btn_forward, btn_reverse, btn_stop, btn_register):
        btn["state"] = button_state
################################################################################
def enable_all_buttons(state = True):
    enable_all_direction_buttons(state)
    if state:
         btn_register["state"] = tk.NORMAL
    else:
         btn_register["state"] = tk.DISABLED
################################################################################
def inputbox_register_name_validate(P):
    """ Disable direction buttons when user name not provided"""
    # P is the value of the entry if the edit is allowed so disable all buttons when name is empty
    ##print("inputbox_register_name_validate(), P = " + P) #i = %d, length = %d" % (int(i), length)

    if P != "":
        # Place your code here to enable the register button when input not empty
        pass
    else:
        # Place your code here to disable the register button when input not empty
        pass
    return True
################################################################################
def allow_keyboard_to_control_robot():
    """ map keyboard events to button clicks"""

    # Place your code here to map keyboard keys to button events
    pass
################################################################################
def robot_move(direction):
    """ Move the robot.
    'direction' can be one of "left", "right", "forward, "reverse" or "stop"  """

    response = send_direction_request_to_robot_server(direction)

    # Place your code here to provide feedback to the user about erors etc.

################################################################################
def robot_register():
    """ Register our name with the server so the server knows wher the commands
    are coming from and can restrict to one user at a time """
    return server_request("/robot/api/register?" + urllib.urlencode({'user': inputbox_register_name.get()}))
################################################################################
def btn_left_click():
    """ left button has been clicked """
    # Place your code here to move the robot
    pass
################################################################################
def btn_right_click():
    """ right button has been clicked """
    # Place your code here to move the robot
    pass
################################################################################
def btn_forward_click():
    """ forward button has been clicked """
    # Place your code here to move the robot
    pass
################################################################################
def btn_reverse_click():
    """ left button has been clicked """
    # Place your code here to move the robot
    pass
################################################################################
def btn_stop_click():
    """ left button has been clicked """
    # Place your code here to move the robot
    pass
################################################################################
def btn_register_click():
    """ left button has been clicked """
    # Place your code here to register the robot
    pass
################################################################################


# Create a window and give it a title
window = tk.Tk()
window.wm_title("Robot Controller")

# Frames are used to group controls on a window.
# Create 2 frames on our window: one for direction buttons and one for register text, input box and register button.
frame_register = ttk.Frame(window, relief=tk.RAISED, borderwidth=1)
frame_register.pack(fill=tk.BOTH, expand=1)
frame_direction_btns = ttk.Frame(window, relief = tk.RAISED, borderwidth=1)
frame_direction_btns.pack(fill=tk.BOTH, expand=1)

# Place the register label text, register input box and register button on the register frame
tk.Label(frame_register, text = "Name:").pack(side = tk.LEFT)
inputbox_register_name = tk.Entry(frame_register, validate="key", validatecommand = (window.register(inputbox_register_name_validate), '%P'))
btn_register = tk.Button(frame_register, text ="register", borderwidth = 5, command = btn_register_click)
btn_register.pack(side = tk.RIGHT)
inputbox_register_name.pack(side = tk.RIGHT)

# Create 5 Robot direction control buttons for left, right, forward, reverse and stop.
# Place them all on the direction frame. The 'command' parameter is a function that gets
# called when the button is clicked.
btn_left = tk.Button(frame_direction_btns, text ="<-- Left", command = btn_left_click)
btn_right = tk.Button(frame_direction_btns, text ="Right -->", command = btn_right_click)
btn_forward = tk.Button(frame_direction_btns, text ="Forward", command = btn_forward_click)
btn_reverse = tk.Button(frame_direction_btns, text ="Reverse", command = btn_reverse_click)
btn_stop = tk.Button(frame_direction_btns, text ="  Stop  ", command = btn_stop_click)

# Place your code here to disable all buttons at start (do this last)


# align the direction buttons
btn_left.pack(side = tk.LEFT)
btn_right.pack(side = tk.RIGHT)
btn_forward.pack(side = tk.TOP)
btn_reverse.pack(side = tk.BOTTOM)
btn_stop.pack()

allow_keyboard_to_control_robot()

# In a GUI type program the "main loop" will call our functions when events happen
window.mainloop()
